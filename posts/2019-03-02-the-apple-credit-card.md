---
title: "THE APPLE CREDIT CARD"
description: "Apple is coming out with a new credit card in 2019."
featured: all
date: 2019-03-02
thumbnail: 'https://s3.amazonaws.com/assets.site.getmny.co/the-apple-credit-card.jpg'
heroImage: 'https://s3.amazonaws.com/assets.site.getmny.co/the-apple-credit-card.jpg'
category:
  - news
authors:
  - Jeff
---
Apple is coming out with a new credit card in 2019, in partnership with Goldman Sachs. *It will earn about 2% cash back on most purchases, possibly more on Apple products and services. It will run on Mastercard.* 

Currently, Apple biggest entry into the credit card space is the **Barclaycard Visa® with Apple Rewards**. It offers *0% APR for up to 18 months* for all qualifying Apple purchases and *[3, 2, 1]* points at *[Apple, restaurants, and all purchases]*, respectively.

### MNY Views
We think the **Apple Credit Card Mastercard®** could be good for someone who uses Apple products for their work, and are able to *pay it off* - like the designer that needs a Macbook Pro for Illustrator or the software engineer that needs a Macbook Pro for coding. As you see, we highly recommend the *Macbook Pro*, because we are current owners and think they are high quality.

**BUT**, if you are not using it for work and cannot pay it off within 18 months, we would stay far away from it. Credit card interest rates are notoriously known to be *over 25%*, and will pay a lot of interest payments.

We see this as a strategic move from Apple, being their first major move into the credit card space. *We think* Apple is going to take a healthy portion of the revenues from the Goldman Sachs credit card partnership - at 1% of revenue, this is estimated to be *$2.85 billion*, since Apple generated *$285 billion* in their latest fiscal year. 

At the same time, this move increase their sales because consumers are able to pay through the *Apple Card Card Mastercard®*. Apple also collects interest from consumers who are unable to pay off their card within 18 months.

#### Official Website
- [https://getmny.co](https://getmny.co)

#### Official Blog
- [https://getmny.co/blog](https://getmny.co/blog)

#### Social Media
- Instagram: [https://www.instagram.com/getmny_co](https://www.instagram.com/getmny_co)

- Facebook: [https://www.facebook.com/officialmnyco](https://www.facebook.com/officialmnyco)

- Pinterest: [https://www.pinterest.com/officialmny](https://www.pinterest.com/officialmny)

- Twitter: [https://twitter.com/getmny_co](https://twitter.com/getmny_co)

- Linkedin: [https://www.linkedin.com/company/officialmny](https://www.linkedin.com/company/officialmny)

#### Contributor thanks

We would like to give special thanks and welcome to [Linda](https://www.instagram.com/lindaaaohvee/) for joining on as our first contributor!

Want to be in the next blog post? Send us an email at [hello@getmny.co](hello@getmny.co)!

