---
title: "THE UBER CREDIT CARD"
description: "The new Uber Visa Card's offers unparalleled 4 percent rewards rate on dining out."
featured: all
date: 2019-03-04
thumbnail: 'https://s3.amazonaws.com/assets.site.getmny.co/the-uber-credit-card.jpg'
heroImage: 'https://s3.amazonaws.com/assets.site.getmny.co/the-uber-credit-card.jpg'
category:
  - news
authors:
  - Jeff
---
The new [Uber Visa Card's](https://www.uber.com/c/uber-credit-card/) offers unparalleled **4 percent** rewards rate on dining out. It also offers a variety of rewards shown below:

Sign-Up Bonus:
- **$100** if you spend $500 in first 90 days

Annual Fees:
- **$0** 

Rewards Rate
- **4%** back *(in points)* on dining out, including UberEATS
- **3%** back on travel and hotels, including AirBNB
- **2%** back on online purchases, including using the Uber App
- **1%** back on other purchases
- **Cell phone protection**, if cell phone bill paid using this card *(Up to **$600** covered if you damage or lose your phone)*

Perks
- **No Foreign Transaction fees** 

#### MNY View
We think the **Uber Visa Card** could be good for someone who like to earn rewards and can pay back their credit card bills quickly to avoid interest fees. What we find most interesting the the **Cell phone protection** offer, which means that Uber will pay up to **$600** if you damage or lose your phone, with the contingency of pay the phone bill with the card. For someone who is trying to save money by **not** eating out a lot, this card may not be as attractive, since the highest rewards offer goes to eating out. 


#### Official Website
- [https://getmny.co](https://getmny.co)

#### Official Blog
- [https://getmny.co/blog](https://getmny.co/blog)

#### Social Media
- Instagram: [https://www.instagram.com/getmny_co](https://www.instagram.com/getmny_co)

- Facebook: [https://www.facebook.com/officialmnyco](https://www.facebook.com/officialmnyco)

- Pinterest: [https://www.pinterest.com/officialmny](https://www.pinterest.com/officialmny)

- Twitter: [https://twitter.com/getmny_co](https://twitter.com/getmny_co)

- Linkedin: [https://www.linkedin.com/company/officialmny](https://www.linkedin.com/company/officialmny)

#### Contributor thanks

We would like to give special thanks and welcome to [Linda](https://www.instagram.com/lindaaaohvee/) for joining on as our first contributor!

Want to be in the next blog post? Send us an email at [hello@getmny.co](hello@getmny.co)!

