---
title: "WELCOME TO MNY"
description: "I'm incredibly excited as I'm starting this new business, blog, and website together."
featured: all
date: 2019-02-27
thumbnail: 'https://s3.amazonaws.com/assets.site.getmny.co/welcome-to-mny.png'
heroImage: 'https://s3.amazonaws.com/assets.site.getmny.co/welcome-to-mny.png'
category:
  - news
authors:
  - Jeff
---
I'm incredibly excited as I'm starting this new business, [blog](https://getmny.co/blog/) and [website](https://getmny.co/) together. I've been developing websites and investing for a couple years now - but never put it all together before. It's time to take a leap and do things that bring my joy. My mission is to share content, that will help one person get ***rich in every aspect of life*** within 6 months. 

What is *MNY*? **MNY is a special place on the Internet for the people who are hungry, who want a chill life, for the dreamers who dream to have their finances all sorted out as well as their health, relationships, and pretty much be very abundant in all aspects of life.** We can all create the life that we imagine and dream of, and by conquering what I think is the baseline need that is finance, you'll have the freedom and time to be creative and empowered and to act on inspiration. When you do things out of inspiration, you tend to be your best self. All of this while creating great relationships and investing in your health to live out your best life, since finances aren't the only riches of life. 

Now, I'd like to take a moment to toast all of you, the one that get this message and even those who don't yet, for we support you. Cheers to your passion, your ideals, and your tenacity in never settling for less. We're in this together. And we're going to kick some serious ass.

#### Official Website
- [https://getmny.co](https://getmny.co)

#### Official Blog
- [https://getmny.co/blog](https://getmny.co/blog)

#### Social Media
- Instagram: [https://www.instagram.com/getmny_co](https://www.instagram.com/getmny_co)

- Facebook: [https://www.facebook.com/officialmnyco](https://www.facebook.com/officialmnyco)

- Pinterest: [https://www.pinterest.com/officialmny](https://www.pinterest.com/officialmny)

- Twitter: [https://twitter.com/getmny_co](https://twitter.com/getmny_co)

- Linkedin: [https://www.linkedin.com/company/officialmny](https://www.linkedin.com/company/officialmny)

#### Contributor thanks

We would like to give special thanks and welcome to [Linda](https://www.instagram.com/lindaaaohvee/) for joining on as our first contributor!

Want to be in the next blog post? Send us an email at [hello@getmny.co](hello@getmny.co)!

