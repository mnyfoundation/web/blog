---
title: Contritubing Guest Posts
layout: Default
authors:
  - Jeff
---

# Contribute to the MNY Blog

Are you as excited about the possibilities of personal finance as we are?

Do you stay up at night thinking about how to grow your investment and what you'd do with all that free time (once your investment grows)?

Are you passionate about sharing your knowledge and experience with others? Do you like to write? **Let’s connect!**

At MNY Inc. we value the knowledge and experience our community shares through contributing to our blog. Writing for the MNY blog is another way to contribute.

We regularly feature original content from guest contributors on our company blog and Medium. Guest contributors write about a variety of topics including:

* Innovative financial life-hacks
* Money success stories 
* How-to invest content

We want to share your expertise with a global audience of pioneers. Besides appearing on our blog, we feature contributors’ posts across our social media channels and occasionally in our newsletter.

Guest posts include a contributor bio and contact information, including links to social media.

# How to Contribute

Drop us a line at media@getmny.co, add an [issue on the blog repo](https://gitlab.com/mnyfoundation/web/blog/) or [submit a pull request on our blog repo](https://gitlab.com/mnyfoundation/web/blog/)!

#### Official Website
- [https://getmny.co](https://getmny.co)

#### Official Blog
- [https://getmny.co/blog](https://getmny.co/blog)

#### Social Media
- Instagram: [https://www.instagram.com/getmny_co](https://www.instagram.com/getmny_co)

- Facebook: [https://www.facebook.com/officialmnyco](https://www.facebook.com/officialmnyco)

- Pinterest: [https://www.pinterest.com/officialmny](https://www.pinterest.com/officialmny)

- Twitter: [https://twitter.com/getmny_co](https://twitter.com/getmny_co)

- Linkedin: [https://www.linkedin.com/company/officialmny](https://www.linkedin.com/company/officialmny)



