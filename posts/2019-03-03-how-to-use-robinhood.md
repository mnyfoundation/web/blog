---
title: "HOW TO USE ROBINHOOD"
description: "Robinhood is an app with no-fee stock and cryptocurrency trading."
featured: all
date: 2019-03-02
thumbnail: 'https://s3.amazonaws.com/assets.site.getmny.co/how-to-use-robinhood.jpg'
heroImage: 'https://s3.amazonaws.com/assets.site.getmny.co/how-to-use-robinhood.jpg'
category:
  - guides-and-tutorials
authors:
  - Jeff
---
Robinhood is an app with **no-fee** stock and cryptocurrency trading. The **no-fee** implications also apply to when you deposit and withdraw money to your bank. You can download Robinhood through the App Store, Google Play, or register through their web page. Receive a free stock when you sign up with my [referral link](share.robinhood.com/jeffred). 

Once you have downloaded Robinhood, open it up.

![robinhood-1](https://s3.amazonaws.com/assets.site.getmny.co/how-to-use-robinhood-1.jpg)

Robinhood will ask you a series of personal question such as your name, residential address, social security number, etc. because whenever you trade, it has tax implications. Once you have filled those out, continue.

![robinhood-2](https://s3.amazonaws.com/assets.site.getmny.co/how-to-use-robinhood-2.jpg)

Robinhood has a large number of banks to connect to. Connect to your bank by logging in.

![robinhood-3](https://s3.amazonaws.com/assets.site.getmny.co/how-to-use-robinhood-3.jpg)

Once you have connected your bank, you can start depositing money. It will take about **3 business days** for your money to transfer from your bank account to the Robinhood app.

![robinhood-4](https://s3.amazonaws.com/assets.site.getmny.co/how-to-use-robinhood-4.jpg)

You can also schedule the deposit to be on a recurring basis. *Skip this step if want to deposit only time.*

![robinhood-5](https://s3.amazonaws.com/assets.site.getmny.co/how-to-use-robinhood-5.jpg)

Once your money comes in, you can start buying stocks. You can search up either the stock symbol or the company's name. Here we are searching up Groupon.

![robinhood-6](https://s3.amazonaws.com/assets.site.getmny.co/how-to-use-robinhood-6.jpg)

Once you have searched up the company stock, simply enter the amount you want to purchase and it will be added to your account.

![robinhood-7](https://s3.amazonaws.com/assets.site.getmny.co/how-to-use-robinhood-7.jpg)

That is it! Happy investing 🎉

#### Official Website
- [https://getmny.co](https://getmny.co)

#### Official Blog
- [https://getmny.co/blog](https://getmny.co/blog)

#### Social Media
- Instagram: [https://www.instagram.com/getmny_co](https://www.instagram.com/getmny_co)

- Facebook: [https://www.facebook.com/officialmnyco](https://www.facebook.com/officialmnyco)

- Pinterest: [https://www.pinterest.com/officialmny](https://www.pinterest.com/officialmny)

- Twitter: [https://twitter.com/getmny_co](https://twitter.com/getmny_co)

- Linkedin: [https://www.linkedin.com/company/officialmny](https://www.linkedin.com/company/officialmny)

#### Contributor thanks

We would like to give special thanks and welcome to [Linda](https://www.instagram.com/lindaaaohvee/) for joining on as our first contributor!

Want to be in the next blog post? Send us an email at [hello@getmny.co](hello@getmny.co)!

