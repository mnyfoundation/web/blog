---
title: "HOW TO GET VIDEO GAME CURRENCY FOR TEENAGERS"
description: "A guide on some simple ways to get video game currency as a teenager."
featured: all
date: 2019-03-03
thumbnail: 'https://s3.amazonaws.com/assets.site.getmny.co/how-to-get-video-game-currency-for-teenagers.gif'
heroImage: 'https://s3.amazonaws.com/assets.site.getmny.co/how-to-get-video-game-currency-for-teenagers.gif'
category:
  - guides-and-tutorials
authors:
  - Jeff
---
Are you a teenager who wants that extra damage boost that a dollar can bring you in the game? Or even a cosmetic change that brings you joy every time you log into the game? Well, this is the guide for you on how to make some spare money for your video game pleasures. 

Let's start with how to buy video game currencies. For this tutorial, I'm going to go in-depth on the video games I have played and am familiar with: Maplestory 2, Brawl Stars, and League of Legends.

### The Currencies
Maplestory 2 offers a variety of ways to purchase NX (their in-game currency), but we'll focus on the easiest way: **Karma Koin**.

![game-1](https://s3.amazonaws.com/assets.site.getmny.co/how-to-get-video-game-currency-as-teenagers-1.jpg)

Brawl Stars' in-game currency are **Gems**, which is through an In-App purchase. 

![game-2](https://s3.amazonaws.com/assets.site.getmny.co/how-to-get-video-game-currency-as-teenagers-2.jpeg)

League of Legends' in-game currency are **Riot Points (RP)**.

![game-3](https://s3.amazonaws.com/assets.site.getmny.co/how-to-get-video-game-currency-as-teenagers-3.png)

### Making Money

Now here comes the *fun* part, how to make money as a *teenager*. 

#### Karma Koin

Karma Koin is found in many retailers, such as:

- 7-Eleven
- GameStop
- Walmart
- Walgreens
- Stater Brothers
- Vons
- Albertsons

*In order to get to these locations, ask your parents or older siblings to drive you or walk you there.*

Our *number one* method to make money to buy Karma Koin is simply doing chores. Many parents give a small amount of money to their teenagers for doing chores such as:

- Washing the dishes
- Doing laundry
- Mowing the lawn *(ask your Dad to show you this)*
- Cleaning up your room

We like doing chores because we find it the easiest way to make money. It can earn you at least a **couple dollars, if not more, for one hour worth of work**. Plus you need your parents to drive you to the store to pick up the Karma Koin card, so finishing your chores will make them more than **happy** for them to do that for you.

#### Gems

For Gems, you can buy Google-play Cards in store to fund your purchase, so chores can be good here too. If chores doesn't work out, we found another great way to make money to get Google Play Credit for ***free***. 

We highly recommend [Google Opinion Rewards](https://play.google.com/store/apps/details?id=com.google.android.apps.paidtasks&hl=en_US), as it gives the most dollars for our time. Google opinion rewards ask you very simple questions and surveys about yourself and all you do is fill it out. The questions come in randomly, but at the peak, I've receive up to 5 questions a day. 

Tips
- Answer honestly *(Google Opinion Rewards will stop giving you questions if you lie)*
- Going to different locations *(going to stores can prompt Google Opinion Rewards to ask you about your experience at the store)*
- Really read the question *(Google Opinion Rewards will sometime put trick questions to see if you're paying attention)*

![game-4](https://s3.amazonaws.com/assets.site.getmny.co/how-to-get-video-game-currency-as-teenager-4.png)

#### Riot Points
Riot Points prepaid card can be purchased at many retailers such as:

- 7-Eleven
- BestBuy
- CVS
- Walgreens
- Walmart
- Target
- GameStop

Since Riot Points can be purchased in stores, we highly recommend chores because you easily earn money and make your parents happy to drive you to the store. 

However, if chores isn't an option we also like [Bing Rewards](). Bing Rewards is a rewards program that gives you points for searching on Bing. You get paid in "points", which can be cashed out to Amazon gift card to buy Riot Points (buy Riot Points prepaid card on Amazon.com). *One point is worth $0.001 (one-tenth of a cent).*

- Each search earns you 5 points at Level 1
- Each search earns you 5 points at Level 2
- Paid up to 10 searches per day (Level 1)
- Paid up to 25 searches per day (Level 2)

Level 1 is what you start out with. In order to get to Level 2, you have to earn 500 points within one month. Continue to earn 500 points a month and you'll remain at Level 2. 

So this means at Level 1 you can earn up to *$0.05* per day, if you complete all your searches. While this does not seem much, it does add up and is a really easy task to do, and basically runs in the background. 

At Level 2, you can earn up to *$0.25*, or a whopping *$7.50* per month as a teenager for searching, which basically runs in the background. 

You can also complete offers within the Microsoft Rewards website (website you used to sign up for Bing Rewards). This earns you up to 50 points per day.

Tips
- We recommend installing Microsoft Edge as it comes pre-installed with Bing *(earns you 150 points per month extra for using Microsoft Edge)*
- If you're using Google Chrome or another browser, you can change your default search engine to Bing
- Completing all search and offers limit each day

By maxing out the paid searches, offers, and using Microsoft Edge this earns you up to *$9.15* per month, a very good amount in our opinion.

Max out paid searches every day - *7,500* points per month
Earn 50 points per day for completing offers - *1,500* points per month
Earn up to 150 points per month for actively browsing with Microsoft Edge - *150* points per month

*Total: 9,150* points per month, or *$9.15*

You need at least *$5 or 5000 points*, before you can cash out to your $5 Amazon gift card to purchase your $5 Riot Points prepaid card. This would only take a bit over 2 weeks by using Bing Rewards.

#### Conclusion
We love chores as it makes you money easily and makes your parents happy. We also like Google Opinion Rewards as it gives one of the best rewards for very quick survey. We also like Bing Rewards because it gives a generous up *(up to $9.15 per month)*, for searching which basically runs in the background. If you have any other creative ways, let us know in the comments section or through our social channels below! 


#### Official Website
- [https://getmny.co](https://getmny.co)

#### Official Blog
- [https://getmny.co/blog](https://getmny.co/blog)

#### Social Media
- Instagram: [https://www.instagram.com/getmny_co](https://www.instagram.com/getmny_co)

- Facebook: [https://www.facebook.com/officialmnyco](https://www.facebook.com/officialmnyco)

- Pinterest: [https://www.pinterest.com/officialmny](https://www.pinterest.com/officialmny)

- Twitter: [https://twitter.com/getmny_co](https://twitter.com/getmny_co)

- Linkedin: [https://www.linkedin.com/company/officialmny](https://www.linkedin.com/company/officialmny)

#### Contributor thanks

We would like to give special thanks and welcome to [Linda](https://www.instagram.com/lindaaaohvee/) for joining on as our first contributor!

Want to be in the next blog post? Send us an email at [hello@getmny.co](hello@getmny.co)!

