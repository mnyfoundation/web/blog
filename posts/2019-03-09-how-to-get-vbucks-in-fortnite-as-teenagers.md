---
title: "HOW TO GET VBUCKS IN FORTNITE AS TEENAGERS"
description: "A guide on some simple ways to V-Bucks in fortnite as a teenager."
featured: all
date: 2019-03-09
thumbnail: 'https://s3.amazonaws.com/assets.site.getmny.co/how-to-get-vbucks-in-fortnite-as-teenagers.gif'
heroImage: 'https://s3.amazonaws.com/assets.site.getmny.co/how-to-get-vbucks-in-fortnite-as-teenagers.gif'
category:
  - guides-and-tutorials
authors:
  - Jeff
---
**You wanna show your mates and every newb out there who's boss.**

You want to fly down into the playing field with a *Raven skin or other legendaries*.

![game-1](https://s3.amazonaws.com/assets.site.getmny.co/how-to-get-vbucks-in-fortnite-as-teenager-1.jpg)

But your wallet and everything in it is saying *"Nuh uh, I'm outta here. You must be crazy."*

![game-2](https://s3.amazonaws.com/assets.site.getmny.co/how-to-get-vbucks-in-fortnite-as-teenagers-2.gif)

### Firstly, Never Do This
Never spend your time looking for V-Bucks generators. These are most likely scams and hacks that can potentially steal your account from you. *Just looking out for you mate 😉.* 

### How to Get V-Bucks in Fortnite Battle Royale: 3 Ways

#### 1. Save the World

You can get free V-Bucks and a login bonus by just logging into into *Save the World* mode every day. Free V-Bucks is attainable after logging in after a certain amount of days **(Day 11 and Day 28)**

#### 2. Completing Challenges

After you've advanced through the main storyline of **Save the World**, you'll get access to **Daily Quests**. Each day you'll receive on Daily Quest that requires you to do a particular task to complete. Here are some recent challenges you can expect:

- Deal damage to opposing players while riding a Zipline
- Deal damage to opposing players who are riding a Zipline
- Get an Elimination with an SMG, Pistol and Sniper Rifle
- Search Chests at Sunny Steps or Fatal Fields

#### 3. Getting Free V-Bucks from Your Battle Pass
It will cost around *950 V-Bucks ($9.50)* to get your Battle Pass, but can yield back over ***100 rewards worth over 25,000 V-Bucks***( how typically takes 75 to 150 hours of play). In summary, the Season 8 rewards from the Battle Pass includes:

- 1300 V-Bucks
- Two pets
- Six Wraps
- Four Axes
- Five emotes
- Two toys
- Four gliders
- Six Back Blings
- Five contrails
- 13 Sprays
- Three music tracks

To get the Battle Pass, jump into the Fortnite Battle Royale and head to the Battle Pass tab.

#### Official Website
- [https://getmny.co](https://getmny.co)

#### Official Blog
- [https://getmny.co/blog](https://getmny.co/blog)

#### Social Media
- Instagram: [https://www.instagram.com/getmny_co](https://www.instagram.com/getmny_co)

- Facebook: [https://www.facebook.com/officialmnyco](https://www.facebook.com/officialmnyco)

- Pinterest: [https://www.pinterest.com/officialmny](https://www.pinterest.com/officialmny)

- Twitter: [https://twitter.com/getmny_co](https://twitter.com/getmny_co)

- Linkedin: [https://www.linkedin.com/company/officialmny](https://www.linkedin.com/company/officialmny)

#### Contributor thanks

We would like to give special thanks and welcome to [Linda](https://www.instagram.com/lindaaaohvee/) for joining on as our first contributor!

Want to be in the next blog post? Send us an email at [hello@getmny.co](hello@getmny.co)!

