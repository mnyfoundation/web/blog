---
title: "MNY MENTIONS + LINDA"
description: "Hello Everyone, I'm Linda."
featured: all
date: 2019-03-01
thumbnail: 'https://s3.amazonaws.com/assets.site.getmny.co/mny-mentions-linda.jpg'
heroImage: 'https://s3.amazonaws.com/assets.site.getmny.co/mny-mentions-linda.jpg'
category:
  - mny-mentions
authors:
  - Linda
---
I'm Linda and I am currently a marketing specialist and graphic designer. I graduated in May 2017 with a Bachelor of Arts in Art with a focus in Graphic Design. I love dogs, and all sorts of animals. Cats included, however I am unfortunately allergic to them. I enjoy watching Chinese TV shows in my free time. I am currently learning more about finances, and look forward to starting on this journey with you all.  

#### Official Website
- [https://getmny.co](https://getmny.co)

#### Official Blog
- [https://getmny.co/blog](https://getmny.co/blog)

#### Social Media
- Instagram: [https://www.instagram.com/getmny_co](https://www.instagram.com/getmny_co)

- Facebook: [https://www.facebook.com/officialmnyco](https://www.facebook.com/officialmnyco)

- Pinterest: [https://www.pinterest.com/officialmny](https://www.pinterest.com/officialmny)

- Twitter: [https://twitter.com/getmny_co](https://twitter.com/getmny_co)

- Linkedin: [https://www.linkedin.com/company/officialmny](https://www.linkedin.com/company/officialmny)

#### Contributor thanks

If you haven't yet, head over and learn more about our first contributor, [Linda](https://www.instagram.com/lindaaaohvee/), on [MNY Mentions](https://getmny.co/blog/mny-mentions-linda/)! 

Want to be in the next blog post? Send us an email at [hello@getmny.co](hello@getmny.co)!

