---
title: "INTRODUCING MNY: A NEW CRYPTOCURRENCY"
description: "Introducing MNY: a new cryptocurrency for rewards."
featured: all
date: 2019-03-04
thumbnail: 'https://s3.amazonaws.com/assets.site.getmny.co/mny-logo.png'
heroImage: 'https://s3.amazonaws.com/assets.site.getmny.co/mny-logo.png'
category:
  - news
authors:
  - Jeff
---
*Note: **[MNY]** represent the cryptocurrency and MNY represents our brand.*

At MNY, we're working towards a better future of finance. A universally accessible alternative for cashing in rewards, **[MNY]** is a new cryptocurrency that allows you to have true ownership of your asset.

The first implementation of **[MNY]** will be through giveaways for our loyal community. Our community will receive **[MNY]** and we, MNY, will honor **[MNY]** as points. **[MNY]** can be exchanged for rewards such as ***Amazon gift cards***.

We know cryptocurrency is new. You might love it, or you might have never heard of it. Either way, our mission at MNY is to provide a platform that lets you get ***rich in all aspects of life***. We're working hard to give you the tools to earn **[MNY]**, while learning about money. 

***Subscribe to our newsletter earn 100 [MNY]! (Email us your ETH address for payment.)***


#### Official Website
- [https://getmny.co](https://getmny.co)

#### Official Blog
- [https://getmny.co/blog](https://getmny.co/blog)

#### Social Media
- Instagram: [https://www.instagram.com/getmny_co](https://www.instagram.com/getmny_co)

- Facebook: [https://www.facebook.com/officialmnyco](https://www.facebook.com/officialmnyco)

- Pinterest: [https://www.pinterest.com/officialmny](https://www.pinterest.com/officialmny)

- Twitter: [https://twitter.com/getmny_co](https://twitter.com/getmny_co)

- Linkedin: [https://www.linkedin.com/company/officialmny](https://www.linkedin.com/company/officialmny)

#### Contributor thanks

We would like to give special thanks and welcome to [Linda](https://www.instagram.com/lindaaaohvee/) for joining on as our first contributor!

Want to be in the next blog post? Send us an email at [hello@getmny.co](hello@getmny.co)!

